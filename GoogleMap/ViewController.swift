//
//  ViewController.swift
//  GoogleMap
//
//  Created by Ly Boung on 11/28/17.
//  Copyright © 2017 Ly Boung. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.stopMonitoringSignificantLocationChanges()
        
        GMSServices.provideAPIKey("AIzaSyCxBWrFVKkJQkkyWKGmNBFh2P8OHu_UakM")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        locationAuthStatus()
        print("Latitute: \(currentLocation.coordinate.latitude)")
        print("Longtitute: \(currentLocation.coordinate.longitude)")
        
        let camera = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 17)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.isTrafficEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        
        view = mapView
    }
    
    
    
    func locationAuthStatus(){
        //Check for authentication if user allow to access their location
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            currentLocation = locationManager.location
        }else {
            locationManager.requestWhenInUseAuthorization()
            locationAuthStatus()
        }
    }
}


extension ViewController {
        
    //Add a marker on hold
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        let marker = GMSMarker(position: coordinate)
        marker.appearAnimation = .pop
        marker.title = "\(coordinate.latitude), \(coordinate.longitude)"
        marker.isFlat = true
        marker.map = mapView
    }
    
    
}



